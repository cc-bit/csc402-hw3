﻿// Christopher Chitwood
// CSC 402
// HW3
// 9/15/2020

module HW3

open System

// Problem 1
let rec count x = function
    | y::ys when x = y -> 1 + (count x ys)
    | _ -> 0;;
// END of Problem 1


// Problem 2
// Function type is slightly off from what it should be.
// Need to figure a way to get it to start with 'a -> bool
let rec keepSatisfiers f = function
    | x::xs -> if (f=x) then
                    x::keepSatisfiers f xs
               else
                   keepSatisfiers f xs
    | [] -> []
    ;;
// END of Problem 2


// Problem 3
let rec pairOfListsToListOfPairs = function
 | (x::xs,y::ys) -> (x,y)::pairOfListsToListOfPairs(xs,ys)
 | ([],ys) -> ([];[])
 | (xs,[]) -> ([];[])
 ;;
// END of Problem 3



// Problem 4
let rec maxOfList = function
    | x::xs -> if(xs > []) then
                    (max (x) (maxOfList xs))
               else
                   x
    | [] -> failwith "max of empty list undefined"
    ;;
// END of Problem 4


// Problem 5
let rec merge xList yList =
 match (xList,yList) with
 | (x::xs,y::ys) ->  if(x < y) then  // It has taken me way longer than it should have to understand if statements in F#
                         x::y::(merge xs ys)  // I know now they are expressions and not statements in F#.
                     else
                         y::x::(merge xs ys)
 | (xs,[])  -> xs
 | ([], ys) -> ys   
 ;;
// END of Problem 5

 
 

[<EntryPoint>]
let main argv =
    0 // return an integer exit code
